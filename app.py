import os
import pandas as pd
from scipy.fft import fft, fftfreq, rfft, rfftfreq
import matplotlib.pyplot as plt
import numpy as np
import csv


DATAS_DIRECTORY = None
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
DATAS_DIR = os.path.join(SCRIPT_DIR, 'datas')
RESULT_DIR = os.path.join(SCRIPT_DIR, 'result')
DATA_FRAME_DICT = {}
SAMPLE_RATE = 2048
DURATION = 2

def main():
  #datas klasörünün varlığını kontrol et
  if not create_dataframe_dict():
    print("DATA_FRAME_DICT CANNOT BE CREATED")
    print("CHECK YOUR 'datas' directory in script folder")
    return False
  #result klasörünün varlığını kontrol et
  if not os.path.exists(RESULT_DIR):
    print('result directory is not found. Created...')
    os.mkdir(RESULT_DIR)
  #DATA_FRAME_DICT her .csv uzantılı dosyanın adı ve içeriğinin pandas dataframe'e
  #çevrilmiş halinden oluşan bir sözlüktür.
  #fname değişkeni dosya adı df pandas dataframe'i temsil eder.
  for fname, df in DATA_FRAME_DICT.items():
    #pandas dataframe'i daha kolay işlem yapmak için numpy array'e çevirdik
    two_d_list = df.values
    #result listemeiz
    two_d_result_list = []
    #her bir satıra gir
    count = 0
    for r in two_d_list:
      #scipy.rfft fonksiyonu listenin ilk yarısına fourier transformu yapar.
      #liste döndürür.
      fr = rfft(r)
      #scipy.rfft karmaşık sayılardan oluşan bir liste döndürüyor. bunların real kısmını aldık.
      frr = fr.real
      #Maksimum 5 değeri bulan fonksiyonlarımız python listesi almaktadır.
      #Bu nedenle numpy array'leri payton listesine çevirdik.
      frrl = frr.tolist()
      #İlk 5 maksimum noktasını aldık.
      #Tüm maksimum noktalarını bulup en büyük 5'ini alanget_max_five metodu da isteğe göre çağrılabilir.
      frrl_max = get_first_five_max(frrl)
      #print(count , " : " ,frrl_max)
      count+=1
      #result listemize attık.
      two_d_result_list.append(frrl_max)
    #result listemizi numpy array'e çevirdik. Böylece numpy kütüphanesinin
    #savetext metodunu kullanarak .csv uzantılı dosyalara yazdırdık.
    res_arr = np.array(two_d_result_list)
    #her input dosyası ile aynı isimde fourier transformunun maksimum
    #değerlerinden oluşan bir .csv dosyası result klasöründe yazılacak.
    np.savetxt(os.path.join(RESULT_DIR, fname), res_arr, delimiter=',')

  

def create_dataframe_dict():
  if not os.path.exists(DATAS_DIR):
    return False
  for fname in os.listdir(DATAS_DIR):
    full_path = os.path.join(DATAS_DIR, fname)
    df = pd.read_csv(full_path, header=None)
    DATA_FRAME_DICT[fname] = df
  return True

def get_first_five_max(arr):
  return_arr = []
  count = 0
  for i in range(1, len(arr)-2):
    if count == 5:
      break
    if check_is_max(arr[i-1], arr[i], arr[i+1]):
      return_arr.append(arr[i])
      count+=1
  return return_arr

def get_max_five(arr):
  return_arr = []
  for i in range(1, len(arr)-2):
    if check_is_max(arr[i-1], arr[i], arr[i+1]):
      return_arr.append(arr[i])
  return_arr.sort(reverse=True)
  return return_arr[:5] if len(return_arr) > 5 else return_arr

def check_is_max(a, b, c):
  return b > a and b > c

if __name__ == '__main__':
  main()